const Card = (props) => (
    <div className="column">
              <div className="card crd">
                <div className="card-content">
                  <div className="media">
                    <div className="media-left">
                      <figure className="image is-48x48">
                        <img src="static/pdf.png" alt="pdf_file" />
                      </figure>
                    </div>
                    <div className="media-content">
                      <p className="title is-4">{props.header}</p>
                      <p className="subtitle is-6">{props.link}</p>
                    </div>
                  </div>
                </div>
                <footer className="card-footer">
                  <a href={props.mainLink} className="card-footer-item" target="_blank">View</a>
                  <a href="#" className="card-footer-item">Share</a>
                </footer>
              </div>

              <style jsx>
                {`
                  .crd{
                    border-radius: 25px;
                    transition: transform .2s;
                    
                  }
                  .card:hover {
                    -ms-transform: scale(1.07); /* IE 9 */
                    -webkit-transform: scale(1.07); /* Safari 3-8 */
                    transform: scale(1.07); 
                  }
                `}
              </style>
            </div>
);

export default Card;