import NextHead from 'next/head';
const Head = (props) => (

    <div>
        <NextHead>
            <link rel="icon" href="static/favicon.png" type="image/png"/>
            <link rel="shortcut icon" href="static/favicon.png" type="image/png" />
            <link rel="stylesheet" href="static/bulma/css/bulma.min.css" />
            <link rel="stylesheet" href="static/bulma/css/bulma.css" />
            <link rel="stylesheet" href="static/bulma/css/bulma.css.map" />
            <title>{props.title}</title>
            <meta content="text/html; charset=UTF-8" />
            <link rel="stylesheet" href="static/fontawesome/css/font-awesome.min.css" />
            <link rel="stylesheet" href="static/fontawesome/css/all.min.css" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="Subhajit Mondal" content="Bookshelf: The book serach engine" />
        </NextHead>
    </div>

);

export default Head;