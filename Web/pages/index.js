// import React from 'react';
import Button from '@material-ui/core/Button';
import NextHead from 'next/head';
import Link from 'next/link';

const index = () => (
    <div>
        <NextHead>
        <link rel="icon" href="static/favicon.png" type="image/png"/>
        <link rel="shortcut icon" href="static/favicon.png" type="image/png" />
            <meta content="text/html; charset=UTF-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <meta name="Subhajit Mondal" content="Bookshelf: The book serach engine" />
            <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet" />
            <link href="static/homepage/css/main.css" rel="stylesheet" />
            <link rel="stylesheet" href="static/bulma/css/bulma.min.css" />
            <link rel="stylesheet" href="static/bulma/css/bulma.css" />
            <link rel="stylesheet" href="static/bulma/css/bulma.css.map" />
            <title>Bookshelf: Book search engine</title>
        </NextHead>
        

        <div className="container one">
          <nav className="navbar" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
              <figure className="image is-46x248">
                <img src="static/logo.png" height="46" width="248" />
              </figure>
            </div>

            <div className="navbar-end">
              <div className="navbar-item">
                <div className="buttons">
                  <a className="button is-primary is-normal is-rounded">
                    <strong>Contribute</strong>
                  </a>                  
                </div>
              </div>
            </div>
          </nav>
        </div>



        <div className="s130">
      <form action="results" method="GET">
        <div className="inner-form">
          <div className="input-field first-wrap">
            <div className="svg-wrapper">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
              </svg>
            </div>
            <input id="search" type="text" name="search" placeholder="What book are you looking for?" pattern="^[^ ].+[^ ]$" required />
          </div>
          <div className="input-field second-wrap">
            <button className="btn-search" type="submit">SEARCH</button>
          </div>
        </div>
        <span className="info">ex. books, pdf, ebooks, doc, docx</span>
      </form>
    </div>

    <footer className="footer">
      <div className="content has-text-centered">
        <p>
          <strong>‹BookShelf›</strong> The Book search engine. The source code is licensed under <a href="https://opensource.org/licenses/GPL-3.0" target="_blank">GPL-3.0</a>. If you have any problem please go through the <Link href="/faq"><a>FAQ section</a></Link>.
        </p>
      </div>
    </footer>

    <script src="static/homepage/js/extention/choices.js"></script>
    
  <style jsx>
    {`
      .one {
        padding-top: 15px; 
        
      }
    `}
  </style>

    </div>
);

export default index;