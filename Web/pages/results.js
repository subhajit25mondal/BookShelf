import Head from '../components/head';
import Link from 'next/link';
import Card from '../components/card';
import fetch from 'isomorphic-unfetch';

const Results = (props) => (
    
<div>
        <Head title={props.search+"- BookShelf Search"} />
        
        <div className="container">
          <nav className="navbar is-light is-fixed-top one" role="navigation" aria-label="main navigation">
            <div className="navbar-brand">
              <figure className="image is-46x248">
                  <Link href="/"><a><img src="static/logo.png" height="46" width="248" /></a></Link>
              </figure>
            </div>

            <div className="">
                <form method="GET" action="results">
                <div className="field is-grouped navbar-item">
                    <p className="control is-expanded">
                        <input className="input is-rounded" name="search" id="search" type="text" defaultValue={props.search} placeholder="Search book here" pattern="^[^ ].+[^ ]$" required />
                    </p>
                    <p className="control">
                        <button className="button is-info is-rounded" type="submit">Search</button>
                    </p>
                </div>
                </form>
            </div>

            
              <div className="navbar-item has-dropdown is-hoverable">
                <a className="navbar-link">
                  Related Search
                </a>
                <div className="navbar-dropdown">

                  


                  {props.show3.map(show=> (
                      
                        <Link key={show} href={"/results?search="+show}>
                            <a className="navbar-item">
                            {show}
                            </a>
                        </Link>
                      
                  ))}

                  

                </div>
              </div>
            
              
            
          </nav>
          </div>


        <div className="container two">
          <div className="level">
            <div className="level-left">
              <div className="level-item">
                <figure className="image">
                  <img src="static/icons/google.png" alt="google" />
                </figure>
              </div>
            </div>
          </div>
          <hr className="three" />


          <div className="columns">

            <Card header={props.show1[0].title.substr(4,44)+'...'} link={props.show1[0].url.substr(0,50)+'...'} mainLink={props.show1[0].url} />
            <Card header={props.show1[1].title.substr(4,44)+'...'} link={props.show1[1].url.substr(0,50)+'...'} mainLink={props.show1[1].url} />
            <Card header={props.show1[2].title.substr(4,44)+'...'} link={props.show1[2].url.substr(0,50)+'...'} mainLink={props.show1[2].url} />

            
          </div>


          <div className="level four">
            <div className="level-left">
              <div className="level-item">
                <figure className="image">
                  <img src="static/icons/bing.png" alt="bing" />
                </figure>
              </div>
            </div>
          </div>
          <hr className="three" />


          <div className="columns">

          <Card header={props.show2[0].title.substr(0,40)+'...'} link={props.show2[0].link.substr(0,50)+'...'} mainLink={props.show2[0].link} />
          <Card header={props.show2[1].title.substr(0,40)+'...'} link={props.show2[1].link.substr(0,50)+'...'} mainLink={props.show2[1].link} />
          <Card header={props.show2[2].title.substr(0,40)+'...'} link={props.show2[2].link.substr(0,50)+'...'} mainLink={props.show2[2].link} />
            
          </div>




          <div className="level four">
            <div className="level-left">
              <div className="level-item">
                <figure className="image">
                  <img src="static/icons/yahoo.png" alt="yahoo" />
                </figure>
              </div>
            </div>
          </div>
          <hr className="three" />


          <div className="columns">

          
            <Card header={props.show1[3].title.substr(4,44)+'...'} link={props.show1[3].url.substr(0,50)+'...'} mainLink={props.show1[3].url} />
            <Card header={props.show1[4].title.substr(4,44)+'...'} link={props.show1[4].url.substr(0,50)+'...'} mainLink={props.show1[4].url} />
            <Card header={props.show1[5].title.substr(4,44)+'...'} link={props.show1[5].url.substr(0,50)+'...'} mainLink={props.show1[5].url} />


          </div>



          <div className="level four">
            <div className="level-left">
              <div className="level-item">
                <figure className="image">
                  <img src="static/icons/duck.png" alt="duckduckgo" />
                </figure>
              </div>
            </div>
          </div>
          <hr className="three" />


          <div className="columns">

          <Card header={props.show2[3].title.substr(0,40)+'...'} link={props.show2[3].link.substr(0,50)+'...'} mainLink={props.show2[3].link} />
          <Card header={props.show2[4].title.substr(0,40)+'...'} link={props.show2[4].link.substr(0,50)+'...'} mainLink={props.show2[4].link} />
          <Card header={props.show2[5].title.substr(0,40)+'...'} link={props.show2[5].link.substr(0,50)+'...'} mainLink={props.show2[5].link} />
            
          </div>

        </div>
      
        <br /><br /><br /><br />



        <footer className="footer">
          <div className="content has-text-centered">
            <p>
              <strong>‹BookShelf›</strong> The Book search engine. The source code is licensed under <a href="https://opensource.org/licenses/GPL-3.0" target="_blank">GPL-3.0</a>. If you have any problem please go through the <Link href="/faq"><a>FAQ section</a></Link>.
            </p>
          </div>
        </footer>


        <style jsx>
            {`
            .one {
                padding-top: 15px; 
                padding-bottom: 15px;
                padding-left: 15%;
            }

            .two {
              padding-top : 135px;
            }
            
            .three {
              background-color: #f0f0f0;
            }

            .four {
              padding-top : 40px;
            }
            `}
        </style>

    </div>

);


Results.getInitialProps = async function(context) {
  const { search } = context.query;

  const res3 = await fetch(`http://bookshelf-core.herokuapp.com/related?q=${search}`);
  const show3 = await res3.json();

  const res1 = await fetch(`https://bookshelf-core.herokuapp.com/duck?q=${search}`);
  const show1 = await res1.json();

  const res2 = await fetch(`https://bookshelf-core.herokuapp.com/bing?q=${search}`);
  const show2 = await res2.json();


  return { show1, show2, show3, search };
};

export default Results;
        

